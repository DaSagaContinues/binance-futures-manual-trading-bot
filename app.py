# Imports and Global Variables
import math
import hmac
import hashlib
import requests
from flask import Flask, request, jsonify

app = Flask(__name__)

BASE_URL = "https://fapi.binance.com"
BINANCE_BROKER_ID = "Broker_ID_here"

# Utility Functions
def calculate_max_trade_slots():
    with open("Tickers.txt", "r") as file:
        symbols = file.read().splitlines()
    return len(symbols)

def fetch_account_info(api_key, secret_key):
    url = f"{BASE_URL}/fapi/v2/account"
    timestamp = int(requests.get(f"{BASE_URL}/fapi/v1/time").json()["serverTime"])

    query_string = f"timestamp={timestamp}&brokerId={BINANCE_BROKER_ID}"
    signature = hmac.new(bytes(secret_key, 'utf-8'), query_string.encode('utf-8'), hashlib.sha256).hexdigest()

    headers = {
        "X-MBX-APIKEY": api_key
    }

    params = {
        "timestamp": timestamp,
        "signature": signature,
        "brokerId": BINANCE_BROKER_ID
    }

    response = requests.get(url, headers=headers, params=params)
    return response.json()


def calculate_position_size_and_slots(api_key, secret_key, symbol, leverage):
    account_info = fetch_account_info(api_key, secret_key)
    usdt_balance = float([asset["balance"] for asset in account_info["assets"] if asset["asset"] == "USDT"][0])
    unrealized_profit = float(account_info["totalUnrealizedProfit"])
    capital = usdt_balance + unrealized_profit

    max_trade_slots = calculate_max_trade_slots()

    if capital / (20 * leverage) < max_trade_slots:
        position_size = 20 * leverage
        trade_slots = math.floor(capital / position_size)
    else:
        position_size = capital / (max_trade_slots * leverage)
        trade_slots = max_trade_slots

    return position_size, trade_slots


def enter_position(api_key, secret_key, symbol, position_size, entry_price_closer, entry_price_further, direction):
    order_id = place_limit_order(api_key, secret_key, symbol, position_size, entry_price_closer, direction)
    trailing_stop_market_order_id = update_to_trailing_stop_market_order(api_key, secret_key, symbol, order_id, entry_price_further)
    return trailing_stop_market_order_id

def place_limit_order(api_key, secret_key, symbol, position_size, entry_price, direction):
    url = f"{BASE_URL}/fapi/v1/order"
    timestamp = int(requests.get(f"{BASE_URL}/fapi/v1/time").json()["serverTime"])
    side = "BUY" if direction == "LONG" else "SELL"
    order_type = "LIMIT"

    query_string = f"symbol={symbol}&side={side}&type={order_type}&quantity={position_size}&price={entry_price}&timeInForce=GTC&timestamp={timestamp}&brokerId={BINANCE_BROKER_ID}"
    signature = hmac.new(bytes(secret_key, 'utf-8'), query_string.encode('utf-8'), hashlib.sha256).hexdigest()
    headers = {
        "X-MBX-APIKEY": api_key
    }
    params = {
        "symbol": symbol,
        "side": side,
        "type": order_type,
        "quantity": position_size,
        "price": entry_price,
        "timeInForce": "GTC",
        "timestamp": timestamp,
        "signature": signature,
        "brokerId": BINANCE_BROKER_ID
    }
    response = requests.post(url, headers=headers, params=params)
    return response.json()["orderId"]

def update_to_trailing_stop_market_order(api_key, secret_key, symbol, order_id, trailing_price):
    cancel_order(api_key, secret_key, symbol, order_id)
    side = "BUY" if direction == "LONG" else "SELL"
    order_type = "TRAILING_STOP_MARKET"
    timestamp = int(requests.get(f"{BASE_URL}/fapi/v1/time").json()["serverTime"])

    query_string = f"symbol={symbol}&side={side}&type={order_type}&callbackRate={trailing_price}&timestamp={timestamp}&brokerId={BINANCE_BROKER_ID}"
    signature = hmac.new(bytes(secret_key, 'utf-8'), query_string.encode('utf-8'), hashlib.sha256).hexdigest()
    headers = {
        "X-MBX-APIKEY": api_key
    }
    params = {
        "symbol": symbol,
        "side": side,
        "type": order_type,
        "callbackRate": trailing_price,
        "timestamp": timestamp,
        "signature": signature,
        "brokerId": BINANCE_BROKER_ID
    }
    response = requests.post(url, headers=headers, params=params)
    return response.json()["orderId"]

def cancel_order(api_key, secret_key, symbol, order_id):
    url = f"{BASE_URL}/fapi/v1/order"
    timestamp = int(requests.get(f"{BASE_URL}/fapi/v1/time").json()["serverTime"])

    query_string = f"symbol={symbol}&orderId={order_id}&timestamp={timestamp}&brokerId={BINANCE_BROKER_ID}"
    signature = hmac.new(bytes(secret_key, 'utf-8'), query_string.encode('utf-8'), hashlib.sha256).hexdigest()
    headers = {
        "X-MBX-APIKEY": api_key
    }
    params = {
        "symbol": symbol,
        "orderId": order_id,
        "timestamp": timestamp,
        "signature": signature,
        "brokerId": BINANCE_BROKER_ID
    }
    response = requests.delete(url, headers=headers, params=params)
    return response.json()


def set_stop_loss(api_key, secret_key, symbol, position_size, stop_loss_price, direction):
    url = f"{BASE_URL}/fapi/v1/order"
    timestamp = int(requests.get(f"{BASE_URL}/fapi/v1/time").json()["serverTime"])
    side = "SELL" if direction == "LONG" else "BUY"
    order_type = "STOP_MARKET"

    query_string = f"symbol={symbol}&side={side}&type={order_type}&quantity={position_size}&stopPrice={stop_loss_price}&timestamp={timestamp}&brokerId={BINANCE_BROKER_ID}"
    signature = hmac.new(bytes(secret_key, 'utf-8'), query_string.encode('utf-8'), hashlib.sha256).hexdigest()

    headers = {
        "X-MBX-APIKEY": api_key
    }

    params = {
        "symbol": symbol,
        "side": side,
        "type": order_type,
        "quantity": position_size,
        "stopPrice": stop_loss_price,
        "timestamp": timestamp,
        "signature": signature,
        "brokerId": BINANCE_BROKER_ID
    }

    response = requests.post(url, headers=headers, params=params)
    return response.json()["orderId"]


def exit_position(api_key, secret_key, symbol, position_size, exit_price_closer, exit_price_further, direction):
    order_id_closer = place_trailing_stop_loss_order(api_key, secret_key, symbol, position_size, exit_price_closer, direction)
    order_id_further = place_trailing_take_profit_order(api_key, secret_key, symbol, position_size, exit_price_further, direction)
    return order_id_closer, order_id_further

def place_trailing_stop_loss_order(api_key, secret_key, symbol, position_size, trailing_percentage, direction):
    url = f"{BASE_URL}/fapi/v1/order"
    timestamp = int(requests.get(f"{BASE_URL}/fapi/v1/time").json()["serverTime"])
    side = "SELL" if direction == "LONG" else "BUY"
    order_type = "TRAILING_STOP_MARKET"

    query_string = f"symbol={symbol}&side={side}&type={order_type}&quantity={position_size}&callbackRate={trailing_percentage}&timestamp={timestamp}&brokerId={BINANCE_BROKER_ID}"
    signature = hmac.new(bytes(secret_key, 'utf-8'), query_string.encode('utf-8'), hashlib.sha256).hexdigest()

    headers = {
        "X-MBX-APIKEY": api_key
    }

    params = {
        "symbol": symbol,
        "side": side,
        "type": order_type,
        "quantity": position_size,
        "callbackRate": trailing_percentage,
        "timestamp": timestamp,
        "signature": signature,
        "brokerId": BINANCE_BROKER_ID
    }

    response = requests.post(url, headers=headers, params=params)
    return response.json()["orderId"]

def place_trailing_take_profit_order(api_key, secret_key, symbol, position_size, trailing_percentage, direction):
    url = f"{BASE_URL}/fapi/v1/order"
    timestamp = int(requests.get(f"{BASE_URL}/fapi/v1/time").json()["serverTime"])
    side = "SELL" if direction == "LONG" else "BUY"
    order_type = "TAKE_PROFIT_MARKET"

    query_string = f"symbol={symbol}&side={side}&type={order_type}&quantity={position_size}&callbackRate={trailing_percentage}&timestamp={timestamp}&brokerId={BINANCE_BROKER_ID}"
    signature = hmac.new(bytes(secret_key, 'utf-8'), query_string.encode('utf-8'), hashlib.sha256).hexdigest()

    headers = {
        "X-MBX-APIKEY": api_key
    }

    params = {
        "symbol": symbol,
        "side": side,
        "type": order_type,
        "quantity": position_size,
        "callbackRate": trailing_percentage,
        "timestamp": timestamp,
        "signature": signature,
        "brokerId": BINANCE_BROKER_ID
    }

    response = requests.post(url, headers=headers, params=params)
    return response.json()["orderId"]


def get_current_price(api_key, symbol):
    url = f"{BASE_URL}/fapi/v1/ticker/price"
    headers = {
        "X-MBX-APIKEY": api_key
    }
    params = {
        "symbol": symbol,
    }

    response = requests.get(url, headers=headers, params=params)
    return float(response.json()["price"])


def cancel_stop_loss(api_key, secret_key, symbol, order_id):
    url = f"{BASE_URL}/fapi/v1/order"
    timestamp = int(requests.get(f"{BASE_URL}/fapi/v1/time").json()["serverTime"])

    query_string = f"symbol={symbol}&orderId={order_id}&timestamp={timestamp}&brokerId={BINANCE_BROKER_ID}"
    signature = hmac.new(bytes(secret_key, 'utf-8'), query_string.encode('utf-8'), hashlib.sha256).hexdigest()

    headers = {
        "X-MBX-APIKEY": api_key
    }

    params = {
        "symbol": symbol,
        "orderId": order_id,
        "timestamp": timestamp,
        "signature": signature,
        "brokerId": BINANCE_BROKER_ID
    }

    response = requests.delete(url, headers=headers, params=params)
    return response.json()


def set_trailing_stop_loss(api_key, secret_key, symbol, position_size, side):
    url = f"{BASE_URL}/fapi/v1/order"
    timestamp = int(requests.get(f"{BASE_URL}/fapi/v1/time").json()["serverTime"])
    order_type = "TRAILING_STOP_MARKET"

    query_string = f"symbol={symbol}&side={side}&type={order_type}&quantity={position_size}&callbackRate={TRAILING_STOP_CALLBACK_RATE}&timestamp={timestamp}&brokerId={BINANCE_BROKER_ID}"
    signature = hmac.new(bytes(secret_key, 'utf-8'), query_string.encode('utf-8'), hashlib.sha256).hexdigest()

    headers = {
        "X-MBX-APIKEY": api_key
    }

    params = {
        "symbol": symbol,
        "side": side,
        "type": order_type,
        "quantity": position_size,
        "callbackRate": TRAILING_STOP_CALLBACK_RATE,
        "timestamp": timestamp,
        "signature": signature,
        "brokerId": BINANCE_BROKER_ID
    }

    response = requests.post(url, headers=headers, params=params)
    return response.json()["orderId"]


def set_trailing_take_profit(api_key, secret_key, symbol, position_size, side):
    url = f"{BASE_URL}/fapi/v1/order"
    timestamp = int(requests.get(f"{BASE_URL}/fapi/v1/time").json()["serverTime"])
    order_type = "TAKE_PROFIT_MARKET"

    query_string = f"symbol={symbol}&side={side}&type={order_type}&quantity={position_size}&callbackRate={TRAILING_TAKE_PROFIT_CALLBACK_RATE}&timestamp={timestamp}&brokerId={BINANCE_BROKER_ID}"
    signature = hmac.new(bytes(secret_key, 'utf-8'), query_string.encode('utf-8'), hashlib.sha256).hexdigest()

    headers = {
        "X-MBX-APIKEY": api_key
    }

    params = {
        "symbol": symbol,
        "side": side,
        "type": order_type,
        "quantity": position_size,
        "callbackRate": TRAILING_TAKE_PROFIT_CALLBACK_RATE,
        "timestamp": timestamp,
        "signature": signature,
        "brokerId": BINANCE_BROKER_ID
    }

    response = requests.post(url, headers=headers, params=params)
    return response.json()["orderId"]


# Flask App Endpoints
    @app.route("/api/add_keys", methods=["POST"])
    def add_keys():
        data = request.json
        api_key = data["api_key"]
        secret_key = data["secret_key"]

        # Load existing keys from the JSON file
        with open("api_keys.json", "r") as f:
            api_keys_data = json.load(f)

        # Check if the given API key already exists in the JSON file
        for keys in api_keys_data["keys"]:
            if keys["api_key"] == api_key:
                return jsonify({"message": "API key already exists"}), 400

        # Add the new API key and secret key to the JSON file
        new_key_data = {"api_key": api_key, "secret_key": secret_key}
        api_keys_data["keys"].append(new_key_data)
        with open("api_keys.json", "w") as f:
            json.dump(api_keys_data, f)

        return jsonify({"message": "API key and secret key added successfully"}), 200

    @app.route("/api/calculate_position_size_and_slots", methods=["GET"])
    def calculate_position_size_and_slots_api():
        data = request.json
        api_key = data["api_key"]
        secret_key = data["secret_key"]
        symbol = data["symbol"]
        leverage = data["leverage"]

        # Fetch account information to calculate available capital
        account_info = fetch_account_info(api_key, secret_key)
        balance = float(account_info["balance"])
        unrealized_profit = float(account_info["unrealizedProfit"])
        capital = balance + unrealized_profit

        # Fetch all USDT trade pairs
        tickers = fetch_all_tickers()
        usdt_tickers = [ticker for ticker in tickers if "USDT" in ticker]

        # Calculate max trade slots and position size
        max_trade_slots = len(usdt_tickers)
        if max_trade_slots == 0:
            return jsonify({"message": "No USDT trade pairs found"}), 400

        if capital <= 0:
            return jsonify({"message": "Insufficient capital"}), 400

        if leverage <= 0:
            return jsonify({"message": "Invalid leverage"}), 400

        if symbol not in usdt_tickers:
            return jsonify({"message": "Invalid symbol"}), 400

        if symbol in open_positions:
            return jsonify({"message": "Position already open for this symbol"}), 400

        if direction not in ["LONG", "SHORT"]:
            return jsonify({"message": "Invalid direction"}), 400

        if entry_price_closer <= 0 or entry_price_further <= 0 or exit_price_closer <= 0 or exit_price_further <= 0:
            return jsonify({"message": "Invalid price"}), 400

        position_size = 20 * leverage
        if max_trade_slots > 0:
            position_size = capital / (max_trade_slots * leverage)
        position_size = round(position_size, 4)

        trade_slots = capital / position_size
        trade_slots = int(trade_slots)

        return jsonify({
            "max_trade_slots": max_trade_slots,
            "position_size": position_size,
            "trade_slots": trade_slots
        }), 200

    @app.route("/api/enter_position", methods=["POST"])
    def enter_position_api():
        data = request.json
        api_key = data["api_key"]
        secret_key = data["secret_key"]
        symbol = data["symbol"]
        entry_price_closer = float(data["entry_price_closer"])
        entry_price_further = float(data["entry_price_further"])
        exit_price_closer = float(data["exit_price_closer"])
        exit_price_further = float(data["exit_price_further"])
        stop_loss_price = float(data["stop_loss_price"])
        leverage = int(data["leverage"])
        direction = data["direction"]

        # Calculate position size and trade slots
        result = calculate_position_size_and_slots(api_key, secret_key, symbol, leverage)
        if result.status_code != 200:
            return result

        position_size = result.json()["position_size"]
        trade_slots = result.json()["trade_slots"]

        # Enter position
        result = enter_position(api_key, secret_key, symbol, position_size, entry_price_closer, entry_price_further,
                                direction)
        if result.status_code != 200:
            return result

        order_id = result.json()["order_id"]

        # Set stop loss order
        result = set_stop_loss(api_key, secret_key, symbol, position_size, stop_loss_price, direction)
        if result.status_code != 200:
            cancel_order(api_key, secret_key, symbol, order_id)
            return result

        stop_loss_order_id = result.json()["order_id"]

        # Set trailing take profit order
        result = set_trailing_take_profit(api_key, secret_key, symbol, position_size, direction)
        if result.status_code != 200:
            cancel_order(api_key, secret_key, symbol, order_id)
            cancel_order(api_key, secret_key, symbol, stop_loss_order_id)
            return result

        take_profit_order_id = result.json()["order_id"]

        # Add position to open positions
        open_positions[symbol] = {
            "direction": direction,
            "position_size": position_size,
            "entry_price_closer": entry_price_closer,
            "entry_price_further": entry_price_further,
            "exit_price_closer": exit_price_closer,
            "exit_price_further": exit_price_further,
            "stop_loss_price": stop_loss_price,
            "leverage": leverage,
            "order_id": order_id,
            "stop_loss_order_id": stop_loss_order_id,
            "take_profit_order_id": take_profit_order_id
        }

        # Return open positions
        return jsonify({"open_positions": open_positions}), 200

    @app.route("/api/set_stop_loss", methods=["POST"])
    def set_stop_loss_api():
        data = request.json
        api_key = data["api_key"]
        secret_key = data["secret_key"]
        symbol = data["symbol"]
        position_size = float(data["position_size"])
        stop_loss_price = float(data["stop_loss_price"])
        direction = data["direction"]

        # Set stop loss order
        result = set_stop_loss(api_key, secret_key, symbol, position_size, stop_loss_price, direction)
        if result.status_code != 200:
            return result

        order_id = result.json()["order_id"]

        # Add stop loss order to open positions
        open_positions[symbol]["stop_loss_price"] = stop_loss_price
        open_positions[symbol]["stop_loss_order_id"] = order_id

        # Return open positions
        return jsonify({"open_positions": open_positions}), 200

    @app.route("/api/exit_position", methods=["POST"])
    def exit_position_api():
        data = request.json
        api_key = data["api_key"]
        secret_key = data["secret_key"]
        symbol = data["symbol"]
        position_size = float(data["position_size"])
        exit_price_closer = float(data["exit_price_closer"])
        exit_price_further = float(data["exit_price_further"])
        direction = data["direction"]

        # Cancel stop loss and take profit orders
        stop_loss_order_id = open_positions[symbol]["stop_loss_order_id"]
        take_profit_order_id = open_positions[symbol]["take_profit_order_id"]
        cancel_order(api_key, secret_key, symbol, stop_loss_order_id)
        cancel_order(api_key, secret_key, symbol, take_profit_order_id)

        # Exit position
        result = exit_position(api_key, secret_key, symbol, position_size, exit_price_closer, exit_price_further,
                               direction)
        if result.status_code != 200:
            return result

        order_id = result.json()["order_id"]

        # Remove position from open positions and add to closed positions
        closed_positions.append(open_positions.pop(symbol))

        # Return open and closed positions
        return jsonify({"open_positions": open_positions, "closed_positions": closed_positions}), 200

    @app.route("/api/cancel_stop_loss", methods=["POST"])
    def cancel_stop_loss_api():
        data = request.json
        api_key = data["api_key"]
        secret_key = data["secret_key"]
        symbol = data["symbol"]

        # Cancel stop loss order
        stop_loss_order_id = open_positions[symbol]["stop_loss_order_id"]
        result = cancel_order(api_key, secret_key, symbol, stop_loss_order_id)
        if result.status_code != 200:
            return result

        # Remove stop loss order from open positions
        open_positions[symbol]["stop_loss_price"] = None
        open_positions[symbol]["stop_loss_order_id"] = None

        # Return open positions
        return jsonify({"open_positions": open_positions}), 200


# Main Function
if __name__ == "__main__":
    app.run(debug=True)

